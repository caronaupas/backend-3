require('dotenv').config();
const express = require('express'); //import
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./user.json');

app.listen(port, function(){
  console.log('NodeJS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//Operacion GET (Collection)
app.get(URL_BASE + 'users',
    function(request, response){
      //response.status(200);
      response.status(200).send(usersFile); // la respuesta (send) al final
});

//Peticion GET a un unico usuario mediante ID (Instancia)
app.get(URL_BASE + 'users/:id',
    function(request, response){
      console.log(request.params.id);
      let pos = request.params.id - 1;
      let respuesta =
          (usersFile[pos] == undefined) ? {"msg":"usuario no existente"} : usersFile[pos];
       let status_code = (usersFile[pos] == undefined ? 404 : 200)
      response.status(status_code).send(respuesta); // la respuesta (send) al final
});

//GET con query string
app.get(URL_BASE + 'usersq',
  function(req, res){
    console.log(req.query.id);
    console.log(req.query.country);
    res.send({"msg":"GET con query"});
});

//Peticion POST a users
app.post(URL_BASE + 'users',
  function(req, res){
    console.log('POST a users');
    let tam = usersFile.length;
    let new_user = {
      "ID":tam + 1,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    console.log(new_user);
    usersFile.push(new_user);
    res.send({"msg":"Usuario creado correctamente"});
});

//Peticion PUT a users
app.put(URL_BASE + 'users/:id',
   function(req, res){
     console.log("PUT /techu/v1/users/:id");
     let idBuscar = req.params.id;
     let updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id);
       if(usersFile[i].id == idBuscar) {
         usersFile[i] = updateUser;
         res.send({"msg" : "Usuario actualizado correctamente."});
       }
     }
     res.send({"msg" : "Usuario no encontrado."});
});

//Peticion DELETE a users (mediante su ID)
app.delete(URL_BASE + 'users/:id',
  function(req, res){
    console.log('DELETE en users');
    let pos = req.params.id-1;
    usersFile.splice(pos, 1);
    res.send({"msg": "Usuario eliminado correctamente"});
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /techu/v1/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.status(201).send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.status(404).send({"msg" : "Login incorrecto."});
        }
      }
    }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
  });
}

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /techu/v1/logout");
    var userId = request.body.id;
    for(us of usersFile) {
      if(us.id == userId) {
        console.log('us.id == userID');
        console.log('us.logged');
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.status(201).send({"msg" : "Logout correcto.", "idUsuario" : us.id});
        } else {
          console.log("Logout incorrecto.");
          response.status(404).send({"msg" : "Logout incorrecto."});
        }
      }
    }
});

//Devolver numero total de usuarios
 app.get(URL_BASE + 'total_users',
   function(request, response){
     let tam = usersFile.length;
     console.log('numero total de usuarios: ' + tam);
     response.status(200).send({"num_usuarios " : tam});
 });
